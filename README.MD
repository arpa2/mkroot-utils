# Utilities to extend RunC and "mkroot"

> *RunC with Busybox can do a lot, but not everything you need.
> These are a few simple utilities that are practical when using it.
> Each of the utilities can be switched on or off by configuring CMake.*

We use RunC heavily in our
["mkroot" container builds](https://gitlab.com/arpa2/mkroot/)
but we sometimes need small utilities like these to be present in
the containers.

By default, the following utilities store in the `/` prefix, but
you can set another `$CMAKE_INSTALL_PREFIX` if you like.

By default, all utilities are installed, but each has an `OPTION`
defined for it, so you can toggle what to use.  (TODO: Add the
utility selection to "mkhere" as installation flavours.)

## Switch userid (and groupid)

`/sbin/chme` will change the userid and possibly the groupid
to the numeric value that you want.  This can only be done by
`root` and, as such, skips most variable influences to aid in
security.  This hard-coding is perfect for generated scripts,
such as in "mkroot" where we use it in `init` lines,

```
::respawn:/sbin/chme @UID_inspircd@:@GID_inspircd@ /usr/sbin/inspircd ...
```

The `${UID_inspircd}` and `${GID_inspircd}` are set by the "mkroot"
environment in response to a configuration statement

```
rootfs_user (inspircd GROUP inspircd
	UID 2001 GID 2001
	HOME /var/lib/inspircd
	SHELL /usr/sbin/nologin
	NOTE "InspIRCd IRC server")
```

When `chme` fails as a child of `init`, it will wait for a few seconds
before reporting the exit code.  This would dump the same message on
the console every few seconds, suitabled for interactive fixing.

Usage rules are:

  * Only works for the `root` user
  * Provide numerical userid (and groupid)
  * The groupid defaults to the same value as the userid
  * You may rely on PATH resolution
  * Change to a userid and default groupid with `/sbin/chme 123 cmd0 arg1...`
  * Change userid with independent groupid with `/sbin/chme 123:456 cmd0 arg1...`

## Run a shell script with locking

`/bin/flash` is the flocking shell.  It runs the configured `FLASHS_SHELL` or
its default `/bin/ash` after obtaining a file lock.  It is used in she-bang
notation instead of the shell itself, and has a filename to flock as its argument,

```
#!/bin/flash /var/run/bar.lock
echo START in $$ with $# and $*
sleep 4
echo ENDED in $$ with $# and $*
```

This lock will block-wait if necessary.  The lock is not shared.

Note that a similar result can be had in a richer environment with

```
#!/usr/bin/env -S flock -F /var/run/bar.lock /bin/ash
echo START in $$ with $# and $*
sleep 4
echo ENDED in $$ with $# and $*
```

An example script would be

```
#!/bin/flash /var/run/bar.lock
echo START in $$ with $# and $*
sleep 4
echo ENDED in $$ with $# and $*
```

tested as

```
/tmp/lockshell first & /tmp/lockshell second run & /tmp/lockshell yet another run
```

which would output something like this; the order may vary and the process
numbers surely will, but the `START` and `ENDED` are necessarily paired:

```
START in 18704 with 1 and first
ENDED in 18704 with 1 and first
START in 18807 with 2 and second run
ENDED in 18807 with 2 and second run
START in 18813 with 3 and yet another run
ENDED in 18813 with 3 and yet another run
```

Another script start that would lock properly, but add some
ballast through `util-linux` and its dependencies, is

```
#!/bin/ash
exec 9>/var/run/bar.lock
flock 9
echo START in $$ with $# and $*
sleep 4
echo ENDED in $$ with $# and $*
```

## Pipe Commands between Bind Mounts

Containers mounting the same portions of a file system
may use a pipe after `mkfifo /path/to/pipe`.  The command
`picoput` writes a set of words to such a pipe, and
`picoget` takes it out.  Shell writes would block on a
pipe, but `picoput` prefers to bail out in error if it
cannot send its message.

**Sending.**
To send a command, say something like

```
picoput /path/to/pipe domain add example.com
```

Commands sent must not exceed the system's
`PIPE_BUF` constant, which POSIX requires to
be at least 512 bytes.  Some formatting applies,
so you need to rely on the command.

It is perfectly valid to send no commands at all.
In this case, a synchronisation signal is sent
without further implications.

**Receiving.**
To receive a command, run a shell with the following
she-bang, which waits for a command on the pipeline,
reconstructs the customary `argc` and `argv[]`
values and then triggers the shell configured as
`PICO_SHELL` or its default, `/bin/ash`.

```
#!/bin/picoget /path/to/pipe
DOMAIN="$1"
ADD="$2"
EXAMPLE_COM="$3"
...
```

Reads are always blocking, so you can safely put a
script like this into your `inittab`, which will
run it over and over again, and only one at a time.

**Variables.**
Although it silently ignores any previously defined
variables, `picoget` will treat an initial sequence
of command words with an embedded equals sign as
variable definitions (*this is really how vaguely
environment variables are defined...*) and adds them
to the environment before running the program.  This
means that you might also send with

```
picoput /path/to/pipe DOMAIN=example.com domain add
```

and the `$DOMAIN` variable would be readily available
in the receiving context, without explicit mapping from
a positional variable.  This is intended as a flexible
mechanism for passing optional data and word lists.
As before, there is no requirement of actually passing
any command words; a set of variable definitions may
add an interesting flavour to your cooking of scripts.

**Shell override.**
If you want to use another shell than the configured
one, you can skip the she-bang notation and instead
add a command and possible initial arguments, like in

```
/bin/picoget /path/to/pipe /usr/bin/lua /path/to/script
```

After receiving the command words `"hello"` `"world"`,
this executes as

```
/usr/bin/lua /path/to/script "hello" "world"
```

**Literal transfer.**
Note that no string interpretation is present anywhere;
what the shell delivers to `picoput` is literally what
gets passed as `argv[]` vector to the command, in this
case Lua, and shells have no custom of evaluating strings
presented in this form (they tend to use `-c` for that).

**Queued transmission.**
For command pipes for which an additional directory named
`/path/to/pipe.queue` was created by the administrator,
the `picoput` command will not report delivery errors but
queue messages via the `piconow` background daemon.  This
daemon starts when queueing but it may be good to also run
it when a system reboots, to deliver the persistent queue
to another containers that were down before the reboot.
Do this with the command `piconow /path/to/pipe` on all
those command pipes that have `/path/to/pipe.queue` added.
You cannot start `piconow` too often; it locks out other
instances and gently exits as soon as no work needs to be
done.  Note that without the `/path/to/pipe.queue`, the
refusal of `picoput` still happens when nobody listens.

