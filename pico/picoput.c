/* picoput.c -- Send a command to a pipe in the filesystem.
 *
 * This sends a series of strings as a command to the pipe whose
 * path is in argv [1].  If the pipe would block, which usually
 * means that it has no attached reader, then this command fails.
 * You can use that to decide about a later attempt, perhaps with
 * exponential fallback.  Take this into account in the atomic
 * nature of your scripts.  Also, try to design commands to be
 * idempotent, so they are safe to send at least once.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <unistd.h>
#include <limits.h>
#include <poll.h>
#include <fcntl.h>
#include <errno.h>


int main (int argc, char *argv []) {
	//
	// Require sufficient arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s /path/to/pipe [args ...]\n", argv [0]);
exit1:
		if (getppid () == 1) {
			sleep (5);
		}
		exit (1);
	}
	char *pipename = argv [1];
	//
	// Combine the commandline to a buffer by ending all
	// words with a NUL character; due to the toggled
	// control between writer and closing reader, and
	// because we can pass the PIPE_BUF size atomically,
	// we can rely on the recipient to learn the size of
	// the PICO message from the transfer itself, so we
	// do not need a size prefix.  We do prefix a NUL
	// character so we can pass an empty command.
	//
	char cmd [PIPE_BUF + 5];
	cmd [0] = '\0';
	int totlen = 1;
	for (int argi = 2; argi < argc; argi++) {
		int len = strlen (argv [argi]);
		if (totlen + len + 1 > PIPE_BUF) {
			fprintf (stderr, "Command exceeds pipe buffer size\n");
			goto exit1;
		}
		memcpy (cmd + totlen, argv [argi], len);
		totlen += len;
		cmd [totlen++] = '\0';
	}
	//
	// Check whether a .queue directory exists for future delivery
	char queuename [PATH_MAX + 5];
	snprintf (queuename, PATH_MAX + 3, "%s.queue", pipename);
	int have_queue = (0 == access (queuename, F_OK));
	if (have_queue && (access (queuename, R_OK | W_OK) != 0)) {
		perror ("Queue not accessible");
		goto exit1;
	}
	//
	// Give a queue, check whether a .queue/inuse file requires queueing
	int have_inuse = 0;
	if (have_queue) {
		char inusename [PATH_MAX + 5];
		snprintf (inusename, PATH_MAX + 3, "%s.queue/inuse", pipename);
		have_inuse = (0 == access (inusename, F_OK));
	}
	//
	// Determine whether to invoke piconow below, for queue delivery
	int use_piconow = have_inuse;
	//
	// Open the pipe in blocking mode -- possibly an anonymous pipe
	int pfd = -1;
	int now_pfd = -1;
	if (!use_piconow) {
		pfd = open (pipename, O_WRONLY | O_NONBLOCK);
	}
	if ((pfd < 0) && have_queue) {
		int pipe_piconow [2];
		if (pipe (pipe_piconow) == 0) {
			/* Setup pfd as an anonymous pipe to piconow */
			pfd = pipe_piconow [1];
			now_pfd = pipe_piconow [0];
			/* Pass message to piconow below */
			use_piconow = 1;
		}
	}
	if (pfd < 0) {
		if (errno == ENXIO) {
			fprintf (stderr, "Nobody currently listens to %s\n", pipename);
		} else {
			perror ("Failed to open pipeline");
		}
		goto exit1;
	}
	//
	// Set the pipe to non-blocking
	int fdfl = fcntl (pfd, F_GETFL);
	fdfl |= O_NONBLOCK;
	if (fcntl (pfd, F_SETFL, fdfl) != 0) {
		perror ("Failed to make the pipeline non-blocking");
		close (pfd);
		goto exit1;
	}
	//
	// Write the buffer in one atomic swoop
	ssize_t written = write (pfd, cmd, totlen);
	//
	// Now wait for the other end to close the PICO file,
	// except when we are writing to yet-to-start piconow
	if (!use_piconow) {
		struct pollfd capture_close = {
			.fd = pfd,
			.events = 0,
			.revents = 0
		};
		do {
			poll (&capture_close, 1, -1);
		} while ((capture_close.revents & (POLLERR | POLLHUP | POLLNVAL)) == 0);
	}
	//
	// Close this PICO file; the other side normally decides when
	close (pfd);
	//
	// Now test if all was actually written out
	if (written < 0) {
		perror ("Failed to write command");
		goto exit1;
	} else if (written != totlen) {
		fprintf (stderr, "INTERNAL ERROR: pico wrote %d out of %d, but should be atomic\n", written, totlen);
		goto exit1;
	}
	//
	// If there is an in-use queue then open piconow with now_pfd as &7
	if (use_piconow) {
		if (now_pfd != 7) {
			if (dup2 (now_pfd, 7) < 0) {
				perror ("Queue delivery pipe failed");
				goto exit1;
			}
			close (now_pfd);
		}
		execlp ("piconow", "piconow", pipename, NULL);
		perror ("Queue delivery to piconow failed");
		goto exit1;
	}
	//
	// Message was delivered, so finish happily
	return 0;
}
