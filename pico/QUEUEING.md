# Queues for PICO Command Pipes

> *Ideally, `picoput` always returns immediately, but
> we also prefer to have delivered a message.  This is
> not possible when nobody is listening.  So we add a
> queue that can take over the responsibility.*

Alongside `/path/to/piconame` there now is a directory
`/path/to/piconame.queue/`
with queued message files, as well as a protective
`/path/to/piconame.queue/inuse` lock file.

Queue entries are ordered by their timestamp, using
one-second resolution to accommodate low-resolution
file systems.  Sequential ordering between same-queue
submissions from externally ordered invocations is
crudely enforced by pausing a submitting process for
one second after queue insertion.

A separate `piconow` program flushes a given cache, and
may be started at any convenient time, which usually
means when `picoput` passes a command through the queue
or when booting a system with PICO queues.

The `piconow` program ends quietly when it cannot take
ownership of the `inuse` file, because another process
is then responsible.  The `piconow` program also ends
when it emptied its queue; at this time it removes the
`inuse` file to clarify that the queue is not needed
for loose messaging.  Effectively, there is one active
process on a queue for as long as it has messages that
it wants to transmit.  This owner spends most of its
time block-waiting for the PICO file.


## Receiving a message

  * Receive as before, using an atomic read (command message
    with a size up to `PIPE_BUF`) under the assumption that
    the remote does not send multiple command messages in
    immediate succession.

  * The receiving side closes its PICO file first.  Unlike
    the read, such closing can be detected by the writing
    side, causing it to also close its PICO file.  This
    allows a tidy tic-toc control between writer and reader.

      - This leaves an option open for future back-and-forth
        communication, for command output or repeated commands
        like on a shell.

  * After reading, it is difficult to fork off `piconow`,
    and it is only reasonable once after the sender has gone
    offline.  It complicates the shell start, which now simply
    overtakes the control from `piconow` through an `exec()`
    call.

  * Triggering `piconow` is could be useful when the reading
    side comes online, to retrieve queued command messages
    from an offline sender, but that is a somewhat extreme
    situation.  The locking discipline should span across
    containers, so it should be possible to run `piconow` on
    both the reading and writing sides.


## Sending a new message

PICO preserves order, because that is easy and often useful.

  * Queueing is only considered when the administrator created
    a `piconame.queue` directory.  Queues are used instead of
    direct sending during existence of `piconame.queue/inuse`,
    because that file will be removed after delivery of the
    last work item in the queue.

  * Without a queue or without `inuse` file, send immedately
    as we did before.  Wait until the reading side closes
    the PICO file, then immediately close the writing side.

      - Future back-and-forth communication would recognise
        if a response was sent instead of PICO closing.

  * To work via the queue, send the message to the write side
    of a new `pipe()` and pass its read side as `&7` to a
    subsequently started `piconow` process.  Queue submissions
    will incur a delay of one second to allow order preservation
    with processes that necessarily follow after the completion
    of this submission.


## Flushing messages

The task of `piconow` is to flush a given queue after taking
ownership via a `flock()` on the `piconame.queue` directory.

There is overlap between `picoput` and `piconow` in their
sending behaviour, but `picoput` uses non-blocking access to
the PICO file, whereas `piconow` uses blocking access.  The
formation of a message from strings is done in `picoput` and
`piconow` merely handles binary messages.

The `piconow` queue runner will go away when the queue is
empty, or when another `piconow` process already holds
ownership of the queue.  The current owner will make a full
iteration over the queue after it drops ownership, and
possibly retry the ownership claim when it finds newly
arrived work.  It is not problematic that multiple `piconow`
processes can iterate over the queue, the pivotal point is
that only one can acquire the `inuse` lock on ownership,
and proceed to sending work items.

 0. If a message can be read from `&7`, write it to the queue.
    The message is considered binary and must not exceed the
    `PIPE_BUF` maximum.  The `piconame.queue` directory must
    exist, or otherwise failure ensues.

 1. Continue as background deamon, detached from the parent.
    The parent will delay for one full second before returning
    control, if a command message was read from `&7`.

 3. Load the `piconame.queue/` directory and sort by time.
    When nothing is found and the `inuse` file is locked,
    unlock it and unlink the file, then repeat the process.
    When nothing is found and the `inuse` file is not locked,
    `exit(0)` with no work to do.

 4. The first entry found opens the `inuse` file in the
    `piconame.queue` directory and tries `flock()` on it;
    failing to gain this lock means that another queue runner
    is already active, which causes an `exit(0)`.

 5. Sort the work items in creation/modification time order.
    Sequentially ordered processes are at least one second
    apart; concurrent time lines will be interleaved and may
    have the same timinig, but where this is the case their
    order did not matter during queueing, and so may be sent
    in any order.

 4. Send the work items on the queue, opening the PICO file
    freshly for each send, using block-waiting, and after
    sending wait for the PICO file to be closed on the
    reading side before also closing the writing side.
    Remove each work item from the queue after it is sent.
    After sending all work items, jump back to load the
    `piconame.queue/` directory.

Note how this process always ends by listing the queue with
no ownership.  This is where other `piconow` processes may
take over.  The trick is to allow this, and mitigate the
risk of multiple queue runners with `flock()` on the `inuse`
file.  Processes only take ownership with `flock()` when
they found some work to do, and they return to iterating
the queue until it is empty; effectively, they take up the
responsibility to guide the queue until it is empty, but
during a lapse of the control they tolerate if another
process takes over the responsibility.


## Booting a Container

The `flock()` regime makes it simple to start `piconow` as
often as desired, including during a queueing `picoput` call.
It also makes sense at boot time on the writing side, and
sometimes on the reading side, because that can deliver work
to another side that may have come online while the booting
system was down.

It does not really matter if the queue runner is the
responsibility of the writing side or the reading side, as
long as they share the `piconame.queue/` directory; this
would be the case if the PICO file `piconame` is in a shared
directory; it is not the case however when the PICO file is
shared between file systems as a hard link.  In such setups,
the writing side should be mostly online.


## Correctness

This is a description of the logic behind the algorithms,
in the hope to show their correctness and what they need
for that.

**Purpose.**
The purpose is:

> *Command messages are delivered as soon as possible,
> and will eventually arrive.*

**Rules.**
The general rule is:

> *When you take ownership over a queue, then you ensure
> delivery of all its messages.*

The only exception to this is when the queue processor
is killed by an administrator or a reboot.  The first is
a deliberate choice, the second can be handled by starting
a new `piconow` queue runner after restart.

An important consideration to avoid race conditions is:

> *After a queue owner gives up ownership, it is still
> responsible for delivery of any command messages that
> were added to the queue but for which the sending
> process could not take ownership of the queue.*

The command sending process plays into this:

> *Command messages are atomically added to the queue,
> followed by either taking ownership for delivery or
> the assurance that another process owns the queue.*

**Implementation.**
The implementation uses a queue directory with an
`inuse` file  over which ownership can be obtained
with a `flock()` call.

The general rule translates to code as

> *After adding work to the queue, fork a `piconow`
> background process that runs until it has emptied
> the queue.*

The race condition avoidance rule translates to code as

> *After removing the `.queue/inuse` file and releasing
> its `flock()` by closing it, make an extra pass
> looking for work items; the process adding work
> also opens and locks the `.queue/inuse` file and even
> tests whether it may hold the lock on a removed file;
> only after it holds the lock can it be sure that the
> file will not be removed by another process.*

**Sending.**
To submit a new command message, the order plays into
the race condition avoidance rule with:

> *New messages are added to the queue with an invalid
> name and then atomically renamed to a valid name.
> At this point, any queue owner may process it.  Then,
> an attempt is made to become the queue owner; failure
> caused by existence of another queue owner is a sign
> of success; success in become the queue owner means
> that complete queue delivery must now be assured.*

**Concurrency.**
It is possible to bypass the queue in certain situations:

> *When the queue directory is empty, it may be bypassed.
> Concurrency may cause several direct delivery attempts
> at the same time, but such attempts have no need for
> ordered delivery.*

There may be failures that can be resolved with the queue:

> *When [non-blocking] delivery without the queue fails,
> then the queue can be used to return success because
> of assured future delivery.*

This resolution is not available when no queue exists; so
sending of a command message may fail in that situation,
which was the old mode of operation for `picoput`.

**Stages.**
This mechanism may be considered to start with an
opportunistic first stage:

> *When the queue is absent or empty, immediate delivery
> can be used, without taking ownership of the queue.
> This stage is a very fast.*

This may fall back to a queued stage if a queue exists,

> *When adding work to the queue, an attempt must be
> made to take ownership and, as a result, accept the
> responsibility for future delivery.  This stage is
> very reliable.*

First, try the fast form and only if that fails use the
reliable form.

**Simplification.**
To simplify the normal/fast direct delivery that can be
used when no queue exists, there is an explicit `inuse`
file in the queue directory.  When this file exists, it
flags that the queue is currently in use, and that
concurrent delivery is not permitted.  This is merely
an optimisation for directory iteration.

The `inuse` file in the queue directory exists when
(a) a queue runner has a `flock()` on the queue, and
(b) tne queue runner will make at least one more full
run over the queue to look for (new) work.

For the queue runner, this means that decoupling from
the queue takes a few steps:

 1. It should first detect an empty directory,
    not just after finishing work, but in general.

 2. Then, it releases the `inuse` file.

 3. Then, it releases the `flock()` on the queue.

 4. Then, it checks that the directory is still
    empty; if not, it tries to be the queue runner
    once more.

Starting as a queue runner does the opposite:

 1. Try to claim the `flock()` on the queue.

 2. Then, create the `inuse` file.

 3. Only now, iterate work items on the queue.
    Repeat until no work is left, then continue
    decoupling as described above.

In return for this effort, sending can sample the
`inuse` file to know about existence of the `flock()`
and required ordered delivery by glancing at the
`inuse` file, that is, without grabbing the `flock()`
and gaining the responsibilities associated with it:

 1. Apply direct sending if permitted.  Stop if the
    result is satisfactory.

 2. Atomically insert an element in the queue.

 3. Try to become the queue runner, but be satisfied
    when the `flock()` on the queue is already owned.

