/* piconow.c -- Consider starting a queue runner one a PICO file.
 *
 * After optionally queueing a command file from &7, this program
 * looks for work items in the queue.  If any work is found, it
 * tries to take ownership of the queue runner responsibilities.
 * At most one piconow program at a time can be responsible for
 * a given queue.  After releasing ownership but before letting
 * go completely, another run is made to test for command files
 * that may have been added by a competitive process that could
 * not gain ownership, and so assumes that this piconow process
 * will complete its work too.  If so, ownership is once again
 * sought to deliver the newly found items.  Nothing gets lost in
 * this way, but we only start a queue runner if we have command
 * files in a queue that we would like to deliver.  Such runners
 * mostly block-wait on the reading side of the PICO file.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>
#include <dirent.h>
#include <syslog.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>



typedef struct work_todo {
	struct work_todo *next;
	char qname [8];
	time_t qtime;
} work_todo;


int main (int argc, char *argv []) {
	//
	// INITIALISATION.  Arguments, chacks.  Take in optional message on &7.
	//
	// Check arguments
	char *errmsg = NULL;
	if (argc != 2) {
		fprintf (stderr, "Usage: %s /path/to/pipe\nHint:  This is a queue processor.  Use picoput to send a command\n", argv [0]);
		goto exit1;
	}
	char *piconame = argv [1];
	int piconamelen = strlen (piconame);
	if ((piconamelen < 3) || (piconamelen > PATH_MAX - 15) || (piconame [piconamelen-1] == '/')) {
		fprintf (stderr, "%s: Invalid piconame: %s\n", argv [1]);
		goto exit1;
	}
	if (access (piconame, W_OK) < 0) {
		perror ("No access to command pipe");
		goto exit1;
	}
	//
	// Construct the .queue and .queue/inuse names
	char inusename [PATH_MAX + 5];
	char queuename [PATH_MAX + 5];
	memcpy (queuename, piconame, piconamelen);
	memcpy (queuename + piconamelen, ".queue", 7);
	memcpy (inusename, queuename, piconamelen + 6);
	memcpy (inusename + piconamelen + 6, "/inuse", 7);
	//
	// Require existence of the .queue directory
	struct stat st;
	if (stat (queuename, &st) < 0) {
		perror ("Queue not accessible");
		goto exit1;
	}
	if ((st.st_mode & S_IFDIR) == 0) {
		perror ("Queue is not a directory");
		goto exit1;
	}
	//
	// Pickup any message that may be waiting on fd &7
	char msg [PIPE_BUF];
	ssize_t msgrdlen = read (7, msg, PIPE_BUF);
	bool addmsg = (msgrdlen > 0);
	if (addmsg) {
		close (7);
		/* Write to a temporary, unrecognised queue filename */
		char dotfn [PATH_MAX + 5];
propose_filename:
		snprintf (dotfn, PATH_MAX + 3, "%s/XXXXXX.", queuename);
		int dotfd = mkstemps (dotfn, 1);
		if (dotfd < 0) {
			perror ("Queue entry create error");
			goto exit1;
		}
		/* Prepare a new name, the actual one without trailing dot */
		char newfn [PATH_MAX + 5];
		strncpy (newfn, dotfn, piconamelen + 6 + 1 + 6);
		if (access (newfn, F_OK) == 0) {
			/* Ran into one of those clumsy inabilities in POSIX semantics */
			close (dotfd);
			unlink (dotfn);
			/* Theoretically, we could loop forever on this -- lovely, POSIX */
			goto propose_filename;
		}
		/* We got both unique dotfn and newfn */
		ssize_t written = write (dotfd, msg, msgrdlen);
		close (dotfd);
		if (written != msgrdlen) {
			if (written < 0) {
				perror ("Queue entry write error");
			} else {
				fprintf (stderr, "Queue entry grokked after %d of %d characters; removing\n", written, msgrdlen);
			}
			unlink (dotfn);
			goto exit1;
		}
		/* Move the message to a recognised name (drop the trailing dot) */
		if (rename (dotfn, newfn) != 0) {
			perror ("Queue entry append error");
			unlink (dotfn);
			unlink (newfn);
			goto exit1;
		}
	}
	//
	// DEAMON FORK.  Report OK to the parent, and continue in the background.
	//
	// Fork and exit the parent; the child may continue, if it becomes the owner
	switch (fork ()) {
	case -1:
		/* Fork failed */
		perror ("Queue processing not forked");
		goto exit1;
	case 0:
		/* Fork succeeded, this is the child */
		setsid ();
		close (0);
		close (1);
		close (2);
		break;
	default:
		/* Fork succeeded, this is the parent */
		if (addmsg) {
			sleep (1);	/* Force order, even on lo-res file systems */
		}
		goto exit0;
	}
	//
	// START LOGGGING.
	//
	// Open the log, and write to the console too.
	//
	openlog ("piconow", LOG_CONS | LOG_PID, LOG_DAEMON);
	//
	// WORK LOADING.  Read directory items and `flock()` to commit to work.
	//
	// We do not own the queue process until we open() and flock() it
	int ownfd = -1;
	bool am_owner = false;
	//
	// Open the queue directory
	int qdirh = open (queuename, O_DIRECTORY | O_RDONLY);
	if (qdirh < 0) {
		errmsg = "Queue access handle error";
		goto perror_exit1;
	}
	DIR *qdir;
	int picocount = 0;
look_for_more_work:
	qdir = opendir (queuename);
	if (qdir == NULL) {
		errmsg = "Queue listing error";
		goto perror_exit1;
	}
	//
	// Loop to read directory entries
	work_todo *work = NULL;
	struct dirent *qent;
	while (errno = 0, qent = readdir (qdir), qent != NULL) {
		//
		// Skip . and .. and other non-conforming names
		if (strlen (qent->d_name) != 6) {
			continue;
		}
		//
		// One process needs to take resonsibility as queue runner;
		// if that is not us, offer to take that responsibility
		// or else assume that the current queue runner takes over
		if (!am_owner) {
			/* Disable concurrent/direct sending by having a .queue/inuse file */
			static const int ownxs = S_IRWXU | S_IRWXG;
			static const int ownfl = O_RDWR | O_CREAT;
gain_ownership:
			ownfd = open (inusename, ownfl, ownxs);
			if (ownfd < 0) {
				errmsg = "Queue rejects inuse flag";
				goto perror_exit1;
			}
			/* Offer to be the queue runner */
			if (flock (ownfd, LOCK_EX | LOCK_NB) < 0) {
				if (errno == EWOULDBLOCK) {
					/* Delegate quietly to an existing queue processor */
					goto exit0;
				} else {
					errmsg = "Queue ownership lock-out";
					goto perror_exit1;
				}
			}
			/* Test for race condition: unlink() on the file we flock()ed */
			int ok = 1;
			struct stat stf, stp;
			ok = ok && (fstat (ownfd,     &stf) == 0);
			ok = ok && ( stat (inusename, &stp) == 0);
			ok = ok && (stf.st_dev == stp.st_dev) && (stf.st_ino == stp.st_ino);
			if (!ok) {
				/* Caught the race condition */
				close (ownfd);
				goto gain_ownership;
			}
			/* We own the .queue/inuse path until we unlock or close ownfd */
			am_owner = true;
			syslog (LOG_DEBUG, "Claimed ownership over %s", piconame);
		}
		//
		// Retrieve change time
		struct stat st;
		if (fstatat (qdirh, qent->d_name, &st, 0) < 0) {
			errmsg = "Queue stat error";
			goto perror_exit1;
		}
		//
		// Construct a new queue entry
		work_todo *new = malloc (sizeof (work_todo));
		if (new == NULL) {
			errmsg = "Queue sorting error";
			goto perror_exit1;
		}
		strcpy (new->qname, qent->d_name);
		new->qtime = st.st_mtime;
		//
		// Insert the new entry at the proper position
		work_todo **hdp = &work;
		while ((*hdp != NULL) && ((*hdp)->qtime <= new->qtime)) {
			hdp = &(*hdp)->next;
		}
		new->next = *hdp;
		*hdp = new;
	}
	if (errno != 0) {
		/* There is only one proper way to end iteration */
		errmsg = "Queue iteration error";
		goto perror_exit1;
	}
	closedir (qdir);
	//
	// POSSIBLE END.  We may not have found any work to do
	//
	if (work == NULL) {
		//
		// We went over the directory but found no work;
		// but to avoid race conditions we need to make
		// one queue iteration after dropping ownership
		// locking of the queue
		//
		if (am_owner) {
			/* Enable concurrent/direct submission */
			if (unlink (inusename) != 0) {
				/* Use exit() as a fallback unlock... */
				errmsg = "Dangling queue/inuse file";
				goto perror_exit1;
			}
			/* Let got of the owner file and its lock */
			close (ownfd);
			ownfd = -1;
			am_owner = false;
			syslog (LOG_DEBUG, "Dropped ownership over %s after sending %d messages", piconame, picocount);
			picocount = 0;
			/* Final queue iteration with dropped ownership */
			goto look_for_more_work;
		} else {
			/* No new insertions, so no queue insertion race condition */
			goto exit0;
		}
	}
	//
	// WORK PROCESSING.  Loop over the work list until it is empty.
	//
continue_work:
	//
	// When done sending work items, new work items may have been
	// added and ran into our queue lock; so we continue being the
	// queue runner by making at least one more iteration, for now
	// with the queue lock held.
	if (work == NULL) {
		/* No work left; make one more careful loop */
		goto look_for_more_work;
	}
	//
	// QUEUE PROCESSING.  Insist on completing an owned work item.
	//
	// Gain exclusive access to the command pipe
	int pfd = open (piconame, O_WRONLY | O_EXCL);
	if (pfd < 0) {
		errmsg = "Queue process cannot access PICO command channel";
		goto perror_exit1;
	}
	//
	// Read the first queued work item.  Wait for exclusive access.
	int qfd = openat (qdirh, work->qname, O_RDONLY | O_EXCL);
	if (qfd < 0) {
		/* Perhaps the file is still being written to */
		sleep (2);
		qfd = openat (qdirh, work->qname, O_RDONLY | O_EXCL);
	}
	if (qfd < 0) {
		errmsg = "Queue file error";
		goto perror_exit1;
	}
	char qwork [PIPE_BUF];
	int qwlenin = read (qfd, qwork, PIPE_BUF);
	close (qfd);
	if (qwlenin < 0) {
		errmsg = "Queue work read error";
		goto perror_exit1;
	}
	//
	// Write the work out to the PICO file
	ssize_t qwlenout = write (pfd, qwork, qwlenin);
	//
	// Now wait for the other end to close the PICO file
	struct pollfd capture_close = {
		.fd = pfd,
		.events = 0,
		.revents = 0
	};
	do {
		poll (&capture_close, 1, -1);
	} while ((capture_close.revents & (POLLERR | POLLHUP | POLLNVAL)) == 0);
	//
	// Follow the remote and close this PICO file too
	close (pfd);
	//
	// Now test if all was actually written out
	if (qwlenout < 0) {
		errmsg = "Queue work send error";
		goto perror_exit1;
	} else if (qwlenout != qwlenin) {
		fprintf (stderr, "ERROR: Sent only %d out of %d to PICO channel %s\n", qwlenout, qwlenin, piconame);
		goto exit1;
	}
	//
	// Keep count of the number of messages sent
	picocount++;
	//
	// We can now remove the work item from the queue
	unlinkat (qdirh, work->qname, 0);
	//
	// We can now move ahead to a next work item
	work_todo *oldwork = work;
	work = work->next;
	free (oldwork);
	//
	// Cycle around for more
	goto continue_work;
	//
	// Bail out with an error message
perror_exit1:
	syslog (LOG_ERR, "%s: %s", errmsg, strerror (errno));
	closelog ();
exit1:
	if (getppid () == 1) {
		sleep (10);
	}
	exit (1);
	//
	// Bail out successfully
exit0:
	exit (0);
}

