/* picoget.c -- Read a command from a pipeline and process in a shell
 *
 * The anticipated use of picoget is in a she-bang form,
 *
 *	#!/bin/picoget /path/to/pipe
 *
 * Alternatively, you can explicitly call a shell or command,
 * to which the pipe's command arguments would be added,
 *
 *	/bin/picoget /path/to/pipe [shell] program [args...]
 *
 * It will block-wait until a command enters, setup argc and argv[]
 * and pass it into a shell.  The current script then becomes that
 * shell, which defaults to "/bin/ash" but may be overridden with
 * PICO_SHELL.
 *
 * The command is encoded as a machine-order/size integer, followed
 * by precisely that number of characters holding the arguments
 * (but not argv [0]) with NUL termination characters.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */



#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>


// Following environ(7) but without certain include file
extern char **environ;


#ifndef PICO_SHELL
#define PICO_SHELL "/bin/ash"
#endif


int main (int argc0, char *argv0 []) {
	#ifdef DEBUG
	for (int argi0=0; argi0<=argc0; argi0++) {
		printf ("argv0[%d]=\"%s\"\n", argi0, argv0 [argi0]);
	}
	#endif
	//
	// Need at least the shell and this script, permit another shell
	if (argc0 < 3) {
		fprintf (stderr, "Usage: #!%s /path/to/pipe\n"
			"   or:   %s /path/to/pipe [shell] program [args...]\n",
			 argv0 [0], argv0 [0]);
exit1:
		if (getppid () == 1) {
			sleep (5);
		}
		exit (1);
	}
	char *pipename = argv0 [1];
	//
	// Setup for the PICO_SHELL or the supplied command/args
	char **argv1;
	int argc1;
	char *picoshell_argv0 [] = { PICO_SHELL, argv0 [2], NULL };
	const int picoshell_argc0 = 2;
	if (argc0 == 3) {
		argv1 = picoshell_argv0;
		argc1 = picoshell_argc0;
	} else {
		argv1 = argv0 + 2;
		argc1 = argc0 - 2;
	}
	#ifdef DEBUG
	for (int argi1=0; argi1<=argc1; argi1++) {
		printf ("argv1[%d]=\"%s\"\n", argi1, argv1 [argi1]);
	}
	#endif
	//
	// Determine the size of the original environment
	char **varv0 = environ;
	int varc0 = 0;
	while (varv0 [varc0] != NULL) {
		varc0++;
	}
	//
	// Open the pipe in reading mode, block waiting
	int pfd = open (pipename, O_RDONLY);
	if (pfd < 0) {
		perror ("Failed to open pipeline");
		goto exit1;
	}
	//
	// Read the command
	char cmd [PIPE_BUF + 5];
	int cmdlen = read (pfd, cmd, PIPE_BUF + 3);
	if (cmdlen < 0) {
		perror ("Error reading from pipeline");
		goto exit1;
	}
	//
	// Assure it starts and ends in a NUL character
	if ((cmd [0] != '\0') && (cmd [cmdlen - 1] != '\0')) {
		fprintf (stderr, "Pipeline ABUSE: Command not properly formed\n");
		goto exit1;
	}
	//
	// We will copy all the original environment variables
	// but cmdline overrides seem to add no new functionality
	int varc1 = varc0;
	char **varv1 = varv0;
	//
	// Count the number of arguments and environment variables
	int varc2 = 0;
	int argc2 = 0;
	int inenv = 0;
	int argpos = 1;
	while (argpos < cmdlen) {
		//
		// See if the next word has a '=' before it ends
		size_t namelen = strcspn (cmd + argpos, "=");
		if (cmd [argpos + namelen] == '\0') {
			break;
		}
		//
		// We found '=' so a variable
		varc2++;
		argpos += namelen + 1;
		argpos += strlen (cmd + argpos) + 1;
	}
	while (argpos < cmdlen) {
		//
		// Simply count the remainder as cmdwords
		argc2++;
		size_t wordlen = strlen (cmd + argpos);
		argpos += wordlen + 1;
	}
	//
	// Start v3[] of commandline and variables like v1[]
	char *argv3 [argc1 + argc2 + 1];
	char *varv3 [varc1 + varc2 + 1];
	memcpy (argv3, argv1, argc1 * sizeof (char *));
	memcpy (varv3, varv1, varc1 * sizeof (char *));
	int argc3 = argc1;
	int varc3 = varc1;
	//
	// Now add variables from v2[] to v3[] inasfar as they are new
	argpos = 1;
	for (int i = 0; i < varc2; i++) {
		//
		// Find the namelen
		size_t namelen = strcspn (cmd + argpos, "=");
		//
		// HalfQuadratically search for an existing variable name
		for (int j = 0; j < varc3; j++) {
			if ((memcmp (varv3 [j], cmd + argpos, namelen) == 0) && (varv3 [j] [namelen] == '=')) {
				goto skipvar;
			}
		}
		//
		// If the variable name is new, add it
		varv3 [varc3++] = cmd + argpos;
		//
		// Skip the variable
skipvar:
		argpos += namelen + 1;
		size_t wordlen = strlen (cmd + argpos);
		argpos += wordlen + 1;
	}
	//
	// Now add command words from v2[] to v3[]
	for (int i = 0; i < argc2; i++) {
		//
		// Copy the current word
		argv3 [argc3++] = cmd + argpos;
		//
		// Skip the current word
		size_t wordlen = strlen (cmd + argpos);
		argpos += wordlen + 1;
	}
	//
	// Close off command and variable lists with a NULL
	argv3 [argc3] = NULL;
	varv3 [varc3] = NULL;
	//
	// Invoke the shell configured in PICO_SHELL or the cmdline override
	#ifdef DEBUG
	for (int vari3=0; vari3<=varc3; vari3++) {
		printf ("varv3[%d]=\"%s\"\n", vari3, varv3 [vari3]);
	}
	for (int argi3=0; argi3<=argc3; argi3++) {
		printf ("argv3[%d]=\"%s\"\n", argi3, argv3 [argi3]);
	}
	#endif
	execvpe (argv3 [0], argv3, varv3);
	perror ("Failed to run shell " PICO_SHELL);
	goto exit1;
}
