/* flash.c -- She-bang shell alternative with flock() wrapper.
 *
 * When invoked at the top of a script, as in
 *
 *    #!/usr/bin/flash /var/run/whichever.lock
 *
 * Then this program is called with these two words in argv[0] and
 * argv [1].  Any further commandline information ends up in argv[2]
 * and up.  This means that argv[1] can be flock()ed and then taken
 * away from the array before invoking exec() on FLASH_SHELL, which
 * defaults to "/bin/ash".
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>
#include <fcntl.h>


#ifndef FLASH_SHELL
#define FLASH_SHELL "/bin/ash"
#endif


int main (int argc, char *argv []) {
	//
	// Check arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: #!/bin/flash /var/run/some.lock\n");
exit1:
		if (getppid () == 1) {
			sleep (5);
		}
		exit (1);
	}
	//
	// Open the lock file, possibly creating it
	char *lockn = argv [1];
	int lockf = open (lockn, O_RDWR | O_CREAT, 0660);
	if (lockf < 0) {
		perror ("Failed to open lockfile");
		goto exit1;
	}
	//
	// Claim an exclusive lock, and wait until you have it
	if (flock (lockf, LOCK_EX) != 0) {
		perror ("Lock failed");
		goto exit1;
	}
	//
	// Rewrite arguments; [0] is the FLASH_SHELL, [1..] is old [2..]
	argc -= 1;
	argv [0] = FLASH_SHELL;
	for (int argi = 1; argi < argc; argi++) {
		argv [argi] = argv [argi+1];
	}
	argv [argc] = NULL;
	//
	// Execute the newly formed command
	execvp (FLASH_SHELL, argv);
	perror ("Failed to run " FLASH_SHELL);
	goto exit1;
}
