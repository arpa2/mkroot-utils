/* chme -- change who I am
 *
 * This program is designed to drop privileges in a container
 * such as a RunC environment on BusyBox without workable su.
 * It avoids pulling in extras such as PAM for which the
 * configuration only provides an angle of attack.  This simple
 * program only works when it is called by root, to drop its
 * privileges.  In return, it does not check anything at all.
 * It will of course complain when its rights are wrong.
 *
 * argv[1  ] is UID or UID:GID (in numeric form)
 * argv[2..] is a command to run
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <unistd.h>


int main (int argc, char *argv []) {
	//
	// Only run for root
	if (getuid () != 0) {
		fprintf (stderr, "%s: You are not root\n", argv [0]);
exit1:
		if (getppid () == 1) {
			sleep (5);
		}
		exit (1);
	}
	//
	// Check args
	if (argc < 3) {
usage:
		fprintf (stderr, "Usage: %s uid[:gid] cmd...\n", argv [0]);
		goto exit1;
	}
	//
	// Parse uid or uid:gid
	char *end = NULL;
	unsigned long uid = strtoul (argv [1], &end, 10);
	unsigned long gid = uid;
	if (*end == ':') {
		gid = strtoul (++end, &end, 10);
	}
	if ((*end != '\0') || (uid > 65535) || (gid > 65535)) {
		goto usage;
	}
	//
	// Try to SETUID and SETGID
	int ok = 1;
	ok = ok && (setgid ((gid_t) gid) == 0);
	ok = ok && (getgid () == gid);
	ok = ok && (setuid ((uid_t) uid) == 0);
	ok = ok && (getuid () == uid);
	if (!ok) {
		perror ("Cannot set uid:gid");
		goto exit1;
	}
	//
	// Start the program
	execvp (argv [2], argv+2);
	perror ("Cannot run command");
	goto exit1;
}
